import java.io.PrintWriter;
import java.util.Scanner;

public class Task1991 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        PrintWriter out = new PrintWriter(System.out);

        int n = in.nextInt();
        int k = in.nextInt();
        int notUsed = 0;
        int alive = 0;
        for (int i = 0; i < n; i++) {
            int a = in.nextInt();
            if (a < k)
                alive += k - a;
            else
                notUsed += a - k;
        }
        out.println(notUsed + " " + alive);

        out.flush();
    }
}
