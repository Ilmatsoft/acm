import java.io.PrintWriter;
import java.util.Scanner;
import java.util.Stack;

public class Task1001 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        PrintWriter out = new PrintWriter(System.out);

        Stack<Long> values = new Stack<Long>();
        while (in.hasNextLong())
            values.add(in.nextLong());

        while (!values.isEmpty()) {
            Long value = values.pop();
            out.printf("%1.4f\n", Math.sqrt(value));
        }

        out.flush();
    }
}
