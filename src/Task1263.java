import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Scanner;

public class Task1263 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        PrintWriter out = new PrintWriter(System.out);

        int n = in.nextInt();
        int votes[] = new int[n];
        int m = in.nextInt();
        for (int i = 0; i < m; i++) {
            votes[in.nextInt() - 1]++;
        }
        double total = 0;
        for (int i = 0; i < n; i++)
            total += votes[i];

        for (int i = 0; i < n; i++) {
            out.printf("%.2f%%\n", votes[i] / total * 100.0);
        }

        out.flush();
    }
}
