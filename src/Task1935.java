import java.io.PrintWriter;
import java.util.*;

public class Task1935 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        PrintWriter out = new PrintWriter(System.out);

        int n = in.nextInt();
        List<Integer> list = new ArrayList<Integer>();
        for (int i = 0; i < n; i++) {
            int a = in.nextInt();
            list.add(a);
        }

        Collections.sort(list, new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return Integer.compare(o2, o1);
            }
        });

        int prevA = 0, sum = 0;
        for (Integer a : list) {
            sum += Math.max(a, prevA);
            prevA = a;
        }
        sum += prevA;
        out.println(sum);

        out.flush();
    }
}
