import java.io.PrintWriter;
import java.util.Random;

public class Task1219 {
    private static final int MAX_LENGTH = 1_000_000;
    private static final Random rand = new Random();

    public static void main(String[] args) {
        char letters[] = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};

        PrintWriter out = new PrintWriter(System.out);

        for (int c = 0; c < MAX_LENGTH; c++)
            out.print(letters[rand.nextInt(letters.length)]);

        out.flush();
    }
}
