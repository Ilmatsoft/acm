import java.io.PrintWriter;
import java.util.Scanner;

public class Task1636 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        PrintWriter out = new PrintWriter(System.out);

        int t1 = in.nextInt();
        int t2 = in.nextInt();

        for (int i = 0; i < 10; i++) {
            t2 -= in.nextInt() * 20;
        }
        if (t1 <= t2)
            out.println("No chance.");
        else
            out.println("Dirty debug :(");

        out.flush();
    }
}
