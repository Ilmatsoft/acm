import java.io.PrintWriter;
import java.util.*;

public class Task1777 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        PrintWriter out = new PrintWriter(System.out);

        long a = in.nextLong();
        long b = in.nextLong();
        long c = in.nextLong();

        if (a == b || a == c || b == c)
            out.println(0);

        List<Long> heaps = new ArrayList<Long>();
        heaps.add(a);
        heaps.add(b);
        heaps.add(c);

        Set<Long> diffs = new TreeSet<Long>();
        diffs.add(Math.abs(a - b));
        diffs.add(Math.abs(a - c));
        diffs.add(Math.abs(b - c));

        for (int i = 0; ; i++) {
            long minDiff = diffs.iterator().next();
            if (heaps.contains(minDiff)) {
                out.println(i + 2);
                out.flush();
                return;
            }
            for (long heap : heaps) {
                long diff = Math.abs(heap - minDiff);
                diffs.add(diff);
            }
            heaps.add(minDiff);
        }
    }
}
