import java.io.PrintWriter;
import java.util.Scanner;

public class Task1023_WA2 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        PrintWriter out = new PrintWriter(System.out);

        int k = in.nextInt();
        boolean found = false;
        for (int l = 2; l < k; l++) {
            int steps = (k + l - 1) / l;
            if (steps % 2 == 0) {
                out.print(l);
                found = true;
                break;
            }
        }
        if (!found)
            out.print(0);

        out.flush();
    }
}
