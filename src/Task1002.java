import java.io.PrintWriter;
import java.util.*;

public class Task1002 {
    private static final String NO_SOLUTION = "No solution.";
    private static final Map<String, Integer> charactersDigits = new HashMap<String, Integer>() {{
        put("a", 2);
        put("b", 2);
        put("c", 2);
        put("d", 3);
        put("e", 3);
        put("f", 3);
        put("g", 4);
        put("h", 4);
        put("i", 1);
        put("j", 1);
        put("k", 5);
        put("l", 5);
        put("m", 6);
        put("n", 6);
        put("o", 0);
        put("p", 7);
        put("r", 7);
        put("s", 7);
        put("t", 8);
        put("u", 8);
        put("v", 8);
        put("w", 9);
        put("x", 9);
        put("y", 9);
        put("q", 0);
        put("z", 0);
    }};
    private static Map<String, String> digits2words;
    private static List<String> orderedWordsAsDigits;

    private static String word2digits(String word) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < word.length(); i++)
            sb.append(charactersDigits.get(word.substring(i, i + 1)));
        return sb.toString();
    }

    // array minSize[i] contains minimal number of words needed to get sequence of length i (very strict bound for this branch and bound algorithm)
    private static void bruteForceNumber(String number, String subNumber, Stack<String> sequence, LinkedList<String> minSequence, int[] minSize) {
        if (subNumber.equals(number)) {
            if (minSequence.size() > sequence.size() || minSequence.size() == 0) {
                minSequence.clear();
                minSequence.addAll(sequence);
            }
            return;
        }
        if (subNumber.length() >= number.length()) return;
        if (minSize[subNumber.length()] == 0) minSize[subNumber.length()] = sequence.size();
        if (minSize[subNumber.length()] < sequence.size()) return;

        for (String digits : orderedWordsAsDigits) {
            String _subNumber = subNumber + digits;
            if (_subNumber.length() <= number.length())
                if (number.substring(0, _subNumber.length()).equals(_subNumber))
                    if (sequence.empty() || (!(sequence.peek() + _subNumber).equals(_subNumber + sequence.peek()))) {
                        sequence.push(digits2words.get(digits));
                        bruteForceNumber(number, _subNumber, sequence, minSequence, minSize);
                        sequence.pop();
                    }
        }
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        PrintWriter out = new PrintWriter(System.out);

        while (true) {
            String number = in.next();
            if (number.equals("-1"))
                break;

            digits2words = new HashMap<>();
            int words = in.nextInt();
            for (int i = 0; i < words; i++) {
                String word = in.next();
                String digits = word2digits(word);
                digits2words.put(digits, word);
            }
            orderedWordsAsDigits = new ArrayList<>(digits2words.keySet());
            Collections.sort(orderedWordsAsDigits, new Comparator<String>() {
                @Override
                public int compare(String o1, String o2) {
                    return Integer.compare(o2.length(), o1.length());
                }
            });

            LinkedList<String> minSequence = new LinkedList<>();
            bruteForceNumber(number, "", new Stack<String>(), minSequence, new int[number.length() + 1]);
            if (minSequence.size() > 0) {
                Iterator<String> iterator = minSequence.iterator();
                while (iterator.hasNext()) {
                    out.print(iterator.next());
                    if (iterator.hasNext())
                        out.print(" ");
                }
                out.println();
            } else
                out.println(NO_SOLUTION);
        }

        out.flush();
    }
}
