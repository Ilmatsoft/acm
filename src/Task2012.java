import java.io.PrintWriter;
import java.util.Scanner;

public class Task2012 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        PrintWriter out = new PrintWriter(System.out);

        int f = in.nextInt();
        if (4.0 * 60.0 / 45.0 >= (12-f))
            out.println("YES");
        else
            out.println("NO");

        out.flush();
    }
}
