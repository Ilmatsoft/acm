import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Task1014 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        PrintWriter out = new PrintWriter(System.out);

        long n = in.nextLong();
        if (n == 1) {
            out.print(1);
            out.flush();
            return;
        }
        if (n == 0) {
            out.print(10);
            out.flush();
            return;
        }

        List<Integer> digits = new ArrayList<>();
        for (int d = 9; d > 1; d--) {
            while (n % d == 0) {
                digits.add(d);
                n /= d;
            }
        }
        if (n != 1)
            out.println(-1);
        else {
            for (int i = digits.size() - 1; i >= 0; i--)
                out.print(digits.get(i));
        }

        out.flush();
    }
}
