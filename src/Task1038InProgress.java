import java.io.PrintWriter;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Task1038InProgress {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        PrintWriter out = new PrintWriter(System.out);

        boolean onlineJudge = System.getProperty("ONLINE_JUDGE") != null;

        String text;
        if (onlineJudge) {
            StringBuffer sb = new StringBuffer();
            while (in.hasNextLine()) {
                sb.append(in.nextLine());
                if (in.hasNextLine())
                    sb.append("\n");
            }
            text = sb.toString();
        } else {
            text = "This sentence iz correkt! -It Has,No mista;.Kes et oll.\n" +
                    "But there are two BIG mistakes in this one!\n" +
                    "and here is one more.";
            text = " . this sentence has\n" +
                    "no mistakes at all!";
            text = "Hello! 238 my Friend";
            text="Asf,.,a";
            text="-a.";
            text="-a.d";
            text="This sentence iz correkt! -It Has,No mista;.Kes et\n" +
                    "oll. But there are two\n" +
                    "BIG mistakes in this one!\n" +
                    "and here is one more.";
        }

        int mistakes = 0;
//        boolean sentence = true, word = false;
//
//        byte[] bytes = text.getBytes();
//        for(int i=0;i< bytes.length;i++) {
//            if()
//        }

        Pattern pat0 = Pattern.compile("^([ ,;:-])?[a-z]");
        Matcher match0= pat0.matcher(text);
        while (match0.find()) mistakes++;

        Pattern pat1 = Pattern.compile("^[^a-zA-Z][.!\\?]");
        Matcher match1 = pat1.matcher(text);
        while (match1.find()) mistakes++;

        Pattern pat2 = Pattern.compile("[.!\\?]([ ,;:-])*[a-z]", Pattern.DOTALL | Pattern.MULTILINE);
        Matcher match2 = pat2.matcher(text);
        while (match2.find()) mistakes++;

        Pattern pat3 = Pattern.compile("[a-zA-Z]([A-Z]+)", Pattern.DOTALL | Pattern.MULTILINE);
        Matcher match3 = pat3.matcher(text);
        while (match3.find())
            mistakes += match3.group().length() - 1;

        Pattern pat4 = Pattern.compile("[.!\\?]([ ,;:-])*[0-9]", Pattern.DOTALL | Pattern.MULTILINE);
        Matcher match4 = pat4.matcher(text);
        while (match4.find()) mistakes++;

//        Pattern pat5 = Pattern.compile("[a-zA-Z]\\n[a-z]", Pattern.DOTALL);
//        Matcher match5 = pat5.matcher(text);
//        while (match5.find()) mistakes++;

        out.println(mistakes);

        out.flush();
    }
}
