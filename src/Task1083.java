import java.io.PrintWriter;
import java.util.Scanner;

public class Task1083 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        PrintWriter out = new PrintWriter(System.out);

        int n = in.nextInt();
        int k = in.next().length();

        long fact = 1;
        for (int i = n; i > 0; i -= k)
            fact *= i;

        out.println(fact);

        out.flush();
    }
}
