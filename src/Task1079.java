import java.io.PrintWriter;
import java.util.Scanner;

public class Task1079 {
    private static final int MAX_N = 101_000;

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        PrintWriter out = new PrintWriter(System.out);

        int v[] = new int[MAX_N];
        v[0] = 0;
        v[1] = 1;
        for (int i = 2; i < MAX_N; i++) {
            if (i % 2 == 0)
                v[i] = v[i / 2];
            else
                v[i] = v[i / 2] + v[i / 2 + 1];
        }

        while (true) {
            int n = in.nextInt();
            if (n == 0) break;
            int max = 0;
            for (int i = 0; i <= n; i++)
                if (v[i] > max)
                    max = v[i];
            out.println(max);
        }

        out.flush();
    }
}
