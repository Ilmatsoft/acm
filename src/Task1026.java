import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Task1026 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        PrintWriter out = new PrintWriter(System.out);

        int n = in.nextInt();
        List<Integer> values = new ArrayList<Integer>();
        for (int i = 0; i < n; i++) {
            int v = in.nextInt();
            values.add(v);
        }
        in.next();

        Collections.sort(values);

        int k = in.nextInt();
        for (int i = 0; i < k; i++) {
            int ntx = in.nextInt();
            out.println(values.get(ntx - 1));
        }

        out.flush();
    }
}
