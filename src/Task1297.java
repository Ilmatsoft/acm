import java.io.PrintWriter;
import java.util.Scanner;

public class Task1297 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        PrintWriter out = new PrintWriter(System.out);

        String line = in.next();
        String maxPalindrome = "";
        byte[] bytes = line.getBytes();

        // palindromes of even length
        for (int i = 0; i < bytes.length; i++) {
            int len = 0;
            for (int j = 0; i - j >= 0 && i + j + 1 < bytes.length; j++) {
                if (bytes[i - j] == bytes[i + j + 1])
                    len += 2;
                else
                    break;
            }
            if (len > maxPalindrome.length())
                maxPalindrome = line.substring(i - len / 2 + 1, i + len / 2 + 1);
        }

        // palindromes of odd length
        for (int i = 0; i < bytes.length; i++) {
            int len = -1;
            for (int j = 0; i - j >= 0 && i + j < bytes.length; j++) {
                if (bytes[i - j] == bytes[i + j])
                    len += 2;
                else
                    break;
            }
            if (len > maxPalindrome.length())
                maxPalindrome = line.substring(i - (len + 1) / 2 + 1, i + (len + 1) / 2);
        }

        out.println(maxPalindrome);

        out.flush();
    }
}
