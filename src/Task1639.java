import java.io.PrintWriter;
import java.util.Scanner;

public class Task1639 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        PrintWriter out = new PrintWriter(System.out);

        int n = in.nextInt();
        int m = in.nextInt();
        int cracks = (n-1) + (m-1)*n;
        if(cracks%2==1)
            out.println("[:=[first]");
        else
            out.println("[second]=:]");

        out.flush();
    }
}
