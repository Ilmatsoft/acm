import java.io.PrintWriter;
import java.util.Scanner;

public class Task1931 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        PrintWriter out = new PrintWriter(System.out);

        int n = in.nextInt();
        long a[] = new long[n];
        for (int i = 0; i < n; i++)
            a[i] = in.nextLong();

        int cur = 0, cmp = 0, maxCur = 0, maxCmp = 0;
        for (int i = 1; i < n; i++) {
            if (a[i] > a[cur])
                cmp++;
            else {
                if (cmp > maxCmp) {
                    maxCmp = cmp;
                    maxCur = cur;
                }
                cur = i;
                cmp = 1;
            }
        }
        if (cmp > maxCmp)
            maxCur = cur;
        out.println(maxCur + 1);

        out.flush();
    }
}
