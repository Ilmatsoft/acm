import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Task1306_NoJavaJudge {
    public static void main(String[] args) throws IOException {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            PrintWriter out = new PrintWriter(System.out);

            int n = Integer.valueOf(br.readLine());
            List<Long> values = new ArrayList<>();
            for (int i = 0; i < n; i++) {
                long v = Long.valueOf(br.readLine());
                values.add(v);
            }
            Collections.sort(values);

            if (n % 2 != 0)
                out.println(values.get(n / 2));
            else
                out.println(((double) (values.get(n / 2 - 1)) + (double) (values.get(n / 2))) / 2.0);

            out.flush();
        }
    }
}
