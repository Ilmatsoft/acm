import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Task1086 {
    private static final int MAX_NUMBER = 200_000;

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        PrintWriter out = new PrintWriter(System.out);

        boolean sieve[] = new boolean[MAX_NUMBER + 1];
        for (int i = 0; i < MAX_NUMBER; i++)
            sieve[i] = true;

        List<Integer> primes = new ArrayList<>();
        for (int i = 2; i < MAX_NUMBER; i++) {
            if (sieve[i]) {
                primes.add(i);
                for (int j = 2 * i; j < MAX_NUMBER; j += i)
                    sieve[j] = false;
            }
        }

        int n = in.nextInt();
        for (int i = 0; i < n; i++) {
            int v = in.nextInt();
            out.println(primes.get(v - 1));
        }

        out.flush();
    }
}
