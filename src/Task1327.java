import java.io.PrintWriter;
import java.util.Scanner;

public class Task1327 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        PrintWriter out = new PrintWriter(System.out);

        int a = in.nextInt();
        int b = in.nextInt();
        out.println((b - a) / 2 + (a % 2 + b % 2 + 1) / 2);

        out.flush();
    }
}
