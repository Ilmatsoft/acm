import java.io.PrintWriter;
import java.util.*;

public class Task1452_TLE8 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        PrintWriter out = new PrintWriter(System.out);

        int n = in.nextInt();
        int s[] = new int[n];
        for (int i = 0; i < n; i++)
            s[i] = in.nextInt();

        Map<Integer, Integer> positions = new HashMap<>();
        for (int i = 0; i < n; i++)
            positions.put(s[i], i);

        Set<Integer> distinct = new HashSet<>(positions.keySet());
        List<Integer> orderedValues = new ArrayList<>(distinct);
        Collections.sort(orderedValues);

        int v[] = new int[orderedValues.size()];
        for (int i = 0; i < orderedValues.size(); i++)
            v[i] = orderedValues.get(i);

        int maxLen = 1;
        int maxA1 = v[0];
        int maxD = 1;

        for (int i = 0; i < v.length - maxLen; i++) {
            int a1 = v[i];
            for (int j = i + 1; j < v.length; j++) {
                int a2 = v[j];
                int d = a2 - a1;
                int len = 2;
                for (int k = j + 1; k < v.length; k++) {
                    if (v[k] == a2 + d) {
                        a2 += d;
                        len++;
                    }
                    if (v[k] > a2 + d)
                        break;
                }
                if (len > maxLen) {
                    maxA1 = a1;
                    maxD = d;
                    maxLen = len;
                }
            }
        }

        out.println(maxLen);
        for (int a = maxA1, i = 0; i < maxLen; a += maxD, i++)
            out.print((positions.get(a) + 1) + " ");

        out.flush();
    }
}
