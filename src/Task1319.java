import java.io.PrintWriter;
import java.util.Scanner;

public class Task1319 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        PrintWriter out = new PrintWriter(System.out);

        int n = in.nextInt();
        int shelf[][] = new int[n][n];

        int c = 1;
        for (int i = 0; i <= n; i++)
            for (int j = 0; j < i; j++)
                shelf[j][n - i + j] = c++;

        for (int i = n - 1; i >= 0; i--)
            for (int j = 0; j < i; j++)
                shelf[n - i + j][j] = c++;

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++)
                out.print(shelf[i][j] + " ");
            out.println();
        }

        out.flush();
    }
}
