import java.io.PrintWriter;
import java.util.Scanner;

public class Task1110 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        PrintWriter out = new PrintWriter(System.out);

        int n = in.nextInt();
        int m = in.nextInt();
        int y = in.nextInt();

        int count = 0;
        for (int i = 0; i < m; i++) {
            if (xPowYModZ(i, n, m) == y) {
                out.print(i + " ");
                count++;
            }
        }

        if (count == 0)
            out.println(-1);
        out.flush();
    }

    private static int xPowYModZ(int x, int y, int z) {
        int t = 1;
        for (int j = 0; j < y; j++)
            t = (t * x) % z;
        return t;
    }
}
