import java.io.PrintWriter;
import java.util.Scanner;

public class Task1925 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        PrintWriter out = new PrintWriter(System.out);

        int diff = 0;
        int n = in.nextInt();
        int k = in.nextInt();
        for (int i = 0; i < n; i++) {
            int value = in.nextInt();
            int entered = in.nextInt();
            int expected = value - 2;
            diff += expected - entered;
        }

        int required = k + diff - 2;
        if (required >= 0)
            out.println(required);
        else
            out.println("Big Bang!");

        out.flush();
    }
}
