import java.io.PrintWriter;
import java.util.*;

public class Task1588 {
    private static int n;
    private static int[] x, y;

    private static class Pair {
        int a, b;

        public Pair(int a, int b) {
            if (a < b) {
                this.a = a;
                this.b = b;
            } else {
                this.a = b;
                this.b = a;
            }
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Pair pair = (Pair) o;

            return a == pair.a && b == pair.b;

        }

        @Override
        public int hashCode() {
            int result = a;
            result = 31 * result + b;
            return result;
        }

        @Override
        public String toString() {
            return "[" + a + "," + b + "]";
        }
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        PrintWriter out = new PrintWriter(System.out);

        // read input data
        n = in.nextInt();
        x = new int[n];
        y = new int[n];
        for (int i = 0; i < n; i++) {
            x[i] = in.nextInt();
            y[i] = in.nextInt();
        }

        // put each possible road into own group, all road groups are active - O(n^2)
        Map<Pair, Boolean> active = new HashMap<>();   // is road active
        Map<Pair, Set<Pair>> roads = new HashMap<>();  // roads endpoint cities
        Map<Double, Set<Pair>> roadsAngles = new HashMap<>();    // roads angles
        for (int i = 0; i < n; i++)
            for (int j = 0; j < i; j++) {
                HashSet<Pair> pairs = new HashSet<>();
                Pair pair = new Pair(i, j);
                pairs.add(pair);
                roads.put(pair, pairs);
                active.put(pair, true);
                double angle = Math.atan2(x[pair.a] - x[pair.b], y[pair.a] - y[pair.b]);
                if (angle <= 0) angle += Math.PI;   // angle should be natural (0 should also be PI)
                angle = (double) Math.round(angle * 100000) / 100000;
                if (!roadsAngles.containsKey(angle))
                    roadsAngles.put(angle, new HashSet<Pair>());
                roadsAngles.get(angle).add(pair);
            }

        // merge all road groups into single group if roads are on same line, making second merged group not active
        // (only single group will contains all cities on same line) - formally O(n^2*n^2), but practically should be much less
        for (Set<Pair> roadsSet : roadsAngles.values()) {
            if (roadsSet.size() == 1) continue;
            List<Pair> roadsArray = new ArrayList<>(roadsSet);
            for (int i = 0; i < roadsArray.size(); i++) {
                Pair iPair = roadsArray.get(i);
                if (active.get(iPair))
                    for (int j = i + 1; j < roadsArray.size(); j++) {
                        Pair jPair = roadsArray.get(j);
                        if (active.get(jPair))
                            if (isSameLine(iPair.a, iPair.b, jPair.a, jPair.b)) {
                                roads.get(iPair).addAll(roads.get(jPair));
                                active.put(jPair, false);
                            }
                    }
            }
        }

        // finding maximal distance between cities in each group of cities on same like, aggregating to totalDistance -
        // formally O(n^2*n^2), but practically should be much less
        double totalDistance = 0;
        for (Pair pairKey : roads.keySet()) {
            if (active.get(pairKey)) {
                double maxLength = 0;
                for (Pair pair : roads.get(pairKey)) {
                    double length = distance(pair.a, pair.b);
                    if (length > maxLength)
                        maxLength = length;
                }
                totalDistance += maxLength;
            }
        }
        out.println(Math.round(totalDistance));

        out.flush();
    }

    private static boolean isOnSameLine(int a, int b, int c) {
        return (x[a] - x[c]) * (y[b] - y[c]) - (x[b] - x[c]) * (y[a] - y[c]) == 0.0;
    }

    private static double distance(int a, int b) {
        return Math.sqrt((x[a] - x[b]) * (x[a] - x[b]) + (y[a] - y[b]) * (y[a] - y[b]));
    }

    private static boolean isSameLine(int a, int b, int c, int d) {
        return isOnSameLine(a, b, c) && isOnSameLine(a, b, d);
    }
}
