import java.io.PrintWriter;
import java.util.Scanner;

public class Task1139 {
    public static int gcd(int a, int b) {
        if (b == 0) return a;
        int x = a % b;
        return gcd(b, x);
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        PrintWriter out = new PrintWriter(System.out);

        int n = in.nextInt();
        int m = in.nextInt();
        int gcd = gcd(n - 1, m - 1);

        int _n = (n - 1) / gcd;
        int _m = (m - 1) / gcd;
        out.println(gcd * (_n + _m - 1));

        out.flush();
    }
}
