import java.io.PrintWriter;
import java.util.*;

// квадраты явно могут быть повернуты произвольно
// поэтому нужно найти две другие вершины квадрата, повернув на 90 градусов имеющиеся от центра квадрата)
// расстояния от точки до всех сторон квадрата и углы до всех вершин (чтобы определить сбоку стороны точка или внутри квадрата или под углом)
// можно просто расстояния до всех вершин найти и по ним сделать выводы как расположена точка
public class Task1111_WA3 {
    private static class Square {
        int x1, y1, x2, y2;

        public Square(int x1, int y1, int x2, int y2) {
            if (x1 < x2) {
                this.x1 = x1;
                this.y1 = y1;
                this.x2 = x2;
                this.y2 = y2;
            } else {
                this.x1 = x2;
                this.y1 = y2;
                this.x2 = x1;
                this.y2 = y1;
            }
        }


        @Override
        public String toString() {
            return "[" + x1 + "," + y1 + "," + x2 + "," + y2 + "]";
        }
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        PrintWriter out = new PrintWriter(System.out);

        int n = in.nextInt();
        List<Square> squares = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            int x1 = in.nextInt();
            int y1 = in.nextInt();
            int x2 = in.nextInt();
            int y2 = in.nextInt();
            squares.add(new Square(x1, y1, x2, y2));
        }
        int px = in.nextInt();
        int py = in.nextInt();

        TreeMap<Double, List<Integer>> squaresByDistance = new TreeMap<>();
        for (int i = 0; i < squares.size(); i++) {
            Square square = squares.get(i);
            double distance = distance(square, px, py);
            System.out.println((i+1)+":" +distance);
            distance = Math.round(distance * 100_000_000) / 100_000_000;
            if (!squaresByDistance.containsKey(distance))
                squaresByDistance.put(distance, new ArrayList<Integer>());
            squaresByDistance.get(distance).add(i);
        }

        for (Map.Entry<Double, List<Integer>> entry : squaresByDistance.entrySet()) {
            Collections.sort(entry.getValue());
            for (Integer i : entry.getValue())
                out.print((i + 1) + " ");
        }

        out.flush();
    }

    private static double distance(Square s, int x, int y) {
        if (y < s.y1) {
            if (x < s.x1) return distance(s.x1, s.y1, x, y);
            if (x >= s.x1 && x <= s.x2) return Math.abs(y - s.y1);
            if (x > s.x2) return distance(s.x2, s.y1, x, y);
        }
        if (y >= s.y1 && y <= s.y2) {
            if (x < s.x1) return Math.abs(x - s.x1);
            if (x >= s.x1 && x <= s.x2) return 0;
            if (x > s.x2) return Math.abs(x - s.x2);
        }
        if (y > s.y2) {
            if (x < s.x1) return distance(s.x1, s.y2, x, y);
            if (x >= s.x1 && x <= s.x2) return Math.abs(y - s.y2);
            if (x > s.x2) return distance(s.x2, s.y2, x, y);
        }
        return 0;
    }

    private static double distance(int x1, int y1, int x2, int y2) {
        double dx = x1 - x2;
        double dy = y1 - y2;
        return Math.sqrt(dx * dx + dy * dy);
    }
}

/*
12
-797 -6314 -2827 9400
6599 8354 -1698 -9262
-4367 4707 -6819 -3456
-9081 -1597 -2034 -8071
-5344 -1294 5374 7391
8886 -1433 8797 8123
5755 4629 6117 1844
-9568 -9375 413 -8893
6399 2576 -235 1017
-2189 9272 3306 -1693
8554 -3590 5752 2674
2770 7881 -2439 -3635
2627 2931

is

#1: 0.000000000
#2: 0.000000000
#3: 5317.713157659
#4: 6516.753678021
#5: 0.000000000
#6: 1483.260766015
#7: 1920.539637706
#8: 10242.585388968
#9: 0.000000000
#10: 0.000000000
#11: 3011.068017185
#12: 0.000000000

1 2 5 9 10 12 6 7 11 3 4 8
 */