import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.Set;

public class Task1196 {
    public static void main(String[] args) throws IOException {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            PrintWriter out = new PrintWriter(System.out);

            int n = Integer.valueOf(br.readLine());
            Set<Integer> expectedSet = new HashSet<>();
            for (int i = 0; i < n; i++)
                expectedSet.add(Integer.valueOf(br.readLine()));

            int m = Integer.valueOf(br.readLine());
            int matched = 0;
            for (int i = 0; i < m; i++) {
                if (expectedSet.contains(Integer.valueOf(br.readLine()))) matched++;
            }
            out.println(matched);

            out.flush();
        }
    }
}