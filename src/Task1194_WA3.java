import java.io.PrintWriter;
import java.util.Scanner;

public class Task1194_WA3 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        PrintWriter out = new PrintWriter(System.out);

        int hobbits = in.nextInt();
        int pairs = in.nextInt();



        int totalHandshakes = 0;
        while (in.hasNextInt()) {
            int groupNumber = in.nextInt();
            int subGroups = in.nextInt();
            int subTotal = 1;
            for (int g = 0; g < subGroups; g++) {
                int group = in.nextInt();
                int people = in.nextInt();
                subTotal *= people;
            }
            totalHandshakes += subTotal;
        }

        out.println(totalHandshakes);

        out.flush();
    }
}
