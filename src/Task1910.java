import java.io.PrintWriter;
import java.util.Scanner;

public class Task1910 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        PrintWriter out = new PrintWriter(System.out);

        int n = in.nextInt();
        int power[] = new int[n];
        for (int i = 0; i < n; i++) {
            power[i] = in.nextInt();
        }

        int sum = power[0] + power[1] + power[2];
        int maxSum = sum, maxI = 1;
        for (int i = 2; i < n - 1; i++) {
            sum = sum - power[i - 2] + power[i + 1];
            if (sum > maxSum) {
                maxSum = sum;
                maxI = i;
            }
        }

        out.println(maxSum + " " + (maxI + 1));

        out.flush();
    }
}
