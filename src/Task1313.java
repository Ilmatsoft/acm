import java.io.PrintWriter;
import java.util.Scanner;

public class Task1313 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        PrintWriter out = new PrintWriter(System.out);

        int n = in.nextInt();
        int image[][] = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                image[i][j] = in.nextInt();
            }
        }

        for (int i = 0; i < n; i++)
            for (int j = 0; j <= i; j++)
                out.print(image[i - j][j] + " ");

        for (int i = n - 1; i >= 0; i--)
            for (int j = 0; j < i; j++)
                out.print(image[n - 1 - j][j + n - i] + " ");

        out.flush();
    }
}
