import java.io.PrintWriter;
import java.util.Scanner;

public class Task1082 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        PrintWriter out = new PrintWriter(System.out);

        int n = in.nextInt();
        for (int i = 1; i <= n; i++)
            out.println(i + " ");

        out.flush();
    }
}
