import java.io.*;
import java.util.*;

public class Task1100 {
    public static void main(String[] args) throws IOException {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            PrintWriter out = new PrintWriter(System.out);

            int n = Integer.valueOf(br.readLine());
            TreeMap<Integer, List<Integer>> map = new TreeMap<>(new Comparator<Integer>() {
                @Override
                public int compare(Integer o1, Integer o2) {
                    return Integer.compare(o2, o1);
                }
            });
            for (int i = 0; i < n; i++) {
                String line = br.readLine();
                StringTokenizer st = new StringTokenizer(line);
                int id = Integer.valueOf(st.nextToken());
                int m = Integer.valueOf(st.nextToken());
                if (!map.containsKey(m))
                    map.put(m, new LinkedList<Integer>());
                map.get(m).add(id);
            }

            for (Map.Entry<Integer, List<Integer>> entry : map.entrySet()) {
                Integer m = entry.getKey();
                for (Integer id : entry.getValue())
                    out.println(id + " " + m);
            }

            out.flush();
        }
    }
}
