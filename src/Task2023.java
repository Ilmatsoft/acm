import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Task2023 {
    private static final Map<String, Integer> boxes = new HashMap<String, Integer>() {{
        put("Alice", 1);
        put("Ariel", 1);
        put("Aurora", 1);
        put("Phil", 1);
        put("Peter", 1);
        put("Olaf", 1);
        put("Phoebus", 1);
        put("Ralph", 1);
        put("Robin", 1);

        put("Bambi", 2);
        put("Belle", 2);
        put("Bolt", 2);
        put("Mulan", 2);
        put("Mowgli", 2);
        put("Mickey", 2);
        put("Silver", 2);
        put("Simba", 2);
        put("Stitch", 2);

        put("Dumbo", 3);
        put("Genie", 3);
        put("Jiminy", 3);
        put("Kuzko", 3);
        put("Kida", 3);
        put("Kenai", 3);
        put("Tarzan", 3);
        put("Tiana", 3);
        put("Winnie", 3);
    }};

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        PrintWriter out = new PrintWriter(System.out);

        int n = in.nextInt();
        int lastBox = 1, steps = 0;
        for (int i = 0; i < n; i++) {
            String name = in.next();
            int box = boxes.get(name);
            steps += Math.abs(box - lastBox);
            lastBox = box;
        }
        out.println(steps);

        out.flush();
    }
}
