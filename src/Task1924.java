import java.io.PrintWriter;
import java.util.Scanner;

public class Task1924 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        PrintWriter out = new PrintWriter(System.out);

        int n = in.nextInt();
        long sum = (long) ((1.0 + n) * n / 2.0);
        if (sum % 2 == 0)
            out.println("black");
        else
            out.println("grimy");

        out.flush();
    }
}
