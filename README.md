# README #

This is my small personal archive of solutions for tasks at http://acm.timus.ru/

### How do I get set up? ###

* Every task done as separate executable file (see guide at http://acm.timus.ru/help.aspx?topic=judge)
* List of properly solved tasks avaialbel at http://acm.timus.ru/author.aspx?id=12633
